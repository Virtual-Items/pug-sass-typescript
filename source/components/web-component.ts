/**
 * class WebComponent
 */
 class WebComponent extends HTMLElement
 {
    accesskey: string;
    class: string;
    contenteditable: string;
    dir: string;
    draggable: boolean;
    hidden: boolean;
    id: string;
    lang: string;
    spellcheck: boolean;
    style: CSSStyleDeclaration;
    tabindex: string;
    title: string;
    translate: boolean;
 
    constructor()
    {
        super();
        this.attachShadow({ mode: 'open' });
    }

    static get observedAttributes(): string[]
    {
        return [
            'accesskey',
            'class',
            'contenteditable',
            'dir',
            'draggable',
            'hidden',
            'id',
            'lang',
            'spellcheck',
            'style',
            'tabindex',
            'title',
            'translate'
        ];
    }
 
    get template(): HTMLTemplateElement
    {
        const node = document.createElement('template');
        node.innerHTML = `<h1 id="${this.id || 'title'}"><slot></slot></h1>`;
        return node;
    }

    render()
    {
        this.shadowRoot.innerHTML = '';
        const node = this.template.content.cloneNode(true);
        this.shadowRoot.appendChild(node);
    }

    connectedCallback()
    {
        this.render();
    }

    attributeChangedCallback(name: string, oldValue: string, newValue: string)
    {
        // if (oldValue !== newValue)
        this.render();
    }

    disconnectedCallback()
    {
        //
    }

    adoptedCallback()
    {
        //
    }
 }
 
 export default WebComponent;