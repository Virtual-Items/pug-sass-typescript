const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

// INITIALIZE
const webpackConfig = {};

// PLUGINS
const plugins = [];
plugins.push(new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'source', 'index.pug'),
    filename: 'index.html',
    inject   : true,
    minify: false,
}));
plugins.push(new MiniCssExtractPlugin({
    filename: 'assets/css/bundle.css',
}));
plugins.push(new HtmlWebpackPugPlugin());
plugins.push(new CleanWebpackPlugin());

// RULES
const rules = [];
rules.push({
    test: /\.tsx?$/,
    loader: 'ts-loader',
    exclude: /node_modules/,
});
rules.push({
    test: /\.css$/i,
    use: [MiniCssExtractPlugin.loader, 'css-loader'],
});
rules.push({
    test: /\.sass$/i,
    use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
});
rules.push({
    test: /\.(h|sh){1}tml?$/i,
    include: path.resolve(__dirname, 'source'),
    use: ['html-loader'],
});
rules.push({
    test: /\.pug$/i,
    include: path.resolve(__dirname, 'source'),
    use: ['pug-loader'],
});
rules.push({
    test: /\.(png|jpe?g|gif|svg)$/i,
    loader: 'file-loader',
    options: {
        publicPath: 'assets/images',
        outputPath: 'assets/images',
        name: '[name].[ext]',
        emitFile: true,
        esModule: false,
    },
});

// ENTRY AND OUTPUT
webpackConfig.entry = path.resolve(__dirname, 'source', 'index.ts');

webpackConfig.output = {
    path: path.resolve(__dirname, 'build'),
    filename: 'assets/js/bundle.js',
}

webpackConfig.resolve = {
    extensions: ['.tsx', '.ts', '.js'],
}

webpackConfig.devServer = {
    contentBase: path.resolve(__dirname, 'build'),
    compress: true,
    port: 9000,
};

// ASSEMBLE
webpackConfig.plugins = plugins;
webpackConfig.module = { rules };
webpackConfig.mode = 'production';

// EXPORT THE CONFIGURATION
module.exports = webpackConfig;